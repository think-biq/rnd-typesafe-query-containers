#include <stdbool.h>

#ifndef __NATIVE_PLATFORM_H__
#define __NATIVE_PLATFORM_H__

#define ERROR_SUCCESS 0
#define BOOL bool
#define TRUE true
#define FALSE false
#define ULONG unsigned long
#define UCHAR unsigned char
#define DWORD unsigned int
#define PVOID void*
#define HANDLE void*
#define PHANDLE HANDLE*

#endif