#include "native-platform.h"
#include "native-api.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

const char* CurrrentPlatformDescription = "Some native platform.";

void NativeFreeMemory(void* DataPointer) {
	//
	if (NULL != DataPointer)
		free(DataPointer);
}

DWORD OpenNativeHandle(PHANDLE Handle) {
	return 0;
}

DWORD CloseNativeHandle(HANDLE Handle) {
	return 0;
}

DWORD SomeNativeQueryMethod(HANDLE Handle, NATIVE_OPERATION_CODE Operation, DWORD DataSize, PVOID* Data) {
	DWORD Result = 0;

	switch (Operation) {
	case native_query_is_valid: {
		*Data = malloc(DataSize);
		BOOL* BoolData = *Data;
		*BoolData = true;
	} break;
	case native_query_description: {		
		*Data = malloc(DataSize);
		NATIVE_DESCRIPTION* Description = *Data;

		char* DescriptionContent = (char*)Description->ucDescription;
		strncpy(DescriptionContent
			, CurrrentPlatformDescription
			, NATIVE_DESCRIPTIONT_MAX_LENGTH
		);

		Description->uLength = strlen(DescriptionContent);

		Result = 0 < Description->uLength;
	} break;
	default:
		Result = 1;
	}

	return Result;
}