#pragma once

extern "C" {
#include "native/native-api.h"
}

struct NativeDataContainer {
    void* RawData;
    DWORD Size;

    bool IsValid() const;

    NativeDataContainer();

    NativeDataContainer(DWORD _Size);

    ~NativeDataContainer();
};

class NativeQueryBase {
public:
    const NATIVE_OPERATION_CODE Operation;

    bool HasValidResult() const;

    DWORD GetDataSize() const;

    DWORD Execute(HANDLE Handle);

    NativeQueryBase();

    NativeQueryBase(NATIVE_OPERATION_CODE _Operation, DWORD ContainerSize);

protected:
    NativeDataContainer Result;
};

template<NATIVE_OPERATION_CODE TOperationCode>
class NativeQuery : public NativeQueryBase {
public:
    NativeQuery()
       : NativeQueryBase(TOperationCode, 0)
    {}

    const void* GetResult() { 
        return Result.RawData;
    }
};

#define DECLARE_QUERY_TYPE(OptCode, DataType) \
    template<> class NativeQuery<OptCode> : public NativeQueryBase { \
    public: \
        NativeQuery(); \
        const DataType* GetResult(); \
    };

#define DEFINE_QUERY_TYPE(OptCode, DataType) \
    NativeQuery<OptCode>::NativeQuery() \
        : NativeQueryBase(OptCode, sizeof(DataType)) \
    {} \
    const DataType* NativeQuery<OptCode>::GetResult() { return (DataType*)Result.RawData; } \

DECLARE_QUERY_TYPE(native_query_is_valid, BOOL);
DECLARE_QUERY_TYPE(native_query_description, NATIVE_DESCRIPTION);