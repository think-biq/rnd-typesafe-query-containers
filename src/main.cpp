#include "native-exposure/query.h"
#include <cstdio>

int main () {
    HANDLE Handle;

    if (ERROR_SUCCESS != OpenNativeHandle(&Handle)) {
        // Panik!
        return 1;
    }

    // Execute a query with result type BOOL.
    {
        NativeQuery<NATIVE_OPERATION_CODE::native_query_is_valid> Query;
        DWORD QueryState = Query.Execute(Handle);
        if (Query.HasValidResult()) {
            printf("Is valid query: %s\n", (*Query.GetResult() ? "true" : "false"));
        }
        else {
            printf("Is valid query failed! (%d) DataSize: %d\n", QueryState, Query.GetDataSize());
        }
    }

    // Execute a query with result type NATIVE_DESCRIPTION.
    {
        NativeQuery<NATIVE_OPERATION_CODE::native_query_description> Query;
        DWORD QueryState = Query.Execute(Handle);
        if (Query.HasValidResult()) {
            printf("Description query result: %s\n", Query.GetResult()->ucDescription);
        }
        else {
            printf("Description query failed! (%d) DataSize: %d\n", QueryState, Query.GetDataSize());
        }
    }

    CloseNativeHandle(Handle);

    return 0;
}