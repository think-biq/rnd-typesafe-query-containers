#include "query.h"
#ifdef DEBUG
#include <cstdio>
#endif

/// NativeDataContainer
bool NativeDataContainer::IsValid() const {
    return nullptr != RawData;
}

NativeDataContainer::NativeDataContainer()
    : NativeDataContainer(0)
{}

NativeDataContainer::NativeDataContainer(DWORD _Size)
    : RawData(nullptr)
    , Size(_Size)
{}

NativeDataContainer::~NativeDataContainer() {
#ifdef DEBUG
    printf("Releasing native data container of size %u\n", Size);
#endif
    if (nullptr != RawData) {
        NativeFreeMemory(RawData);
        RawData = nullptr;
        Size = 0;
    }
}
/// NativeDataContainer

/// NativeQueryBase
bool NativeQueryBase::HasValidResult() const {
    return Result.IsValid();
}

DWORD NativeQueryBase::GetDataSize() const { 
    return Result.Size; 
}

DWORD NativeQueryBase::Execute(void* Handle) {
    return SomeNativeQueryMethod(Handle,
        Operation,
        Result.Size, &Result.RawData
    );
}

NativeQueryBase::NativeQueryBase()
    : NativeQueryBase((NATIVE_OPERATION_CODE)-1, 0)
{}

NativeQueryBase::NativeQueryBase(NATIVE_OPERATION_CODE _Operation, DWORD ContainerSize)
    : Operation(_Operation)
    , Result(ContainerSize)
{}
/// NativeQueryBase


/// NativeQuery
DEFINE_QUERY_TYPE(native_query_is_valid, BOOL);
DEFINE_QUERY_TYPE(native_query_description, NATIVE_DESCRIPTION);
///