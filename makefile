# -*- coding: utf8 -*-

FILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
PROJECT_DIR := $(shell dirname $(FILE_PATH))
PROJECT_NAME := $(notdir $(patsubst %/,%,$(dir $(FILE_PATH))))
C := clang
CXX := clang++
EXE := tqt

ifdef (DEBUG)
	C := $(C) -g -DDEBUG
else
	CXX := $(CXX) -g -DDEBUG
endif

.default: debug

debug:
	@echo "FILE_PATH: $(FILE_PATH)"
	@echo "PROJECT_DIR: $(PROJECT_DIR)"
	@echo "PROJECT_NAME: $(PROJECT_NAME)"

prepare:
	mkdir -p tmp
	mkdir -p bin

clean:
	rm -rf tmp
	rm -rf bin

all: build run

build-native-library:
	$(C) -I"src" -shared -fPIC src/native/native-api.c -o bin/libnative.dylib

build-query:
	$(CXX) -I"src" -std=c++14 -c src/native-exposure/query.cpp -o tmp/query.o

build-main:
	$(CXX) -I"src" -std=c++14 src/main.cpp tmp/query.o bin/libnative.dylib -o bin/$(EXE)

build: prepare build-query build-native-library build-main

rebuild: clean prepare build

run:
	bin/./$(EXE)

grind:
	valgrind bin/$(EXE)