#include "native-platform.h"

#ifndef __NATIVE_API_H__
#define __NATIVE_API_H__

#define NATIVE_DESCRIPTIONT_MAX_LENGTH 42
typedef struct _NATIVE_DESCRIPTION {
  ULONG uLength;
  UCHAR ucDescription[NATIVE_DESCRIPTIONT_MAX_LENGTH];
} NATIVE_DESCRIPTION, *PNATIVE_DESCRIPTION;

typedef enum _NATIVE_OPERATION_CODE {
    native_query_is_valid,
    native_query_description

} NATIVE_OPERATION_CODE;

DWORD OpenNativeHandle(PHANDLE Handle);
DWORD CloseNativeHandle(HANDLE Handle);

void NativeFreeMemory(PVOID DataPointer);

DWORD SomeNativeQueryMethod(HANDLE Handle, NATIVE_OPERATION_CODE Operation, DWORD DataSize, PVOID* Data);

#endif